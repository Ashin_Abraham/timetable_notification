from swordfish import SwordFish
from utils import get_required_data, generate_message, get_nomain_message, beautify, configure_bugsnag

configure_bugsnag()


def timetable():
    URL = "https://exams.keralauniversity.ac.in/Login/check3"
    server = SwordFish(URL)
    content = server.get_divContent()
    table = server.get_table_content(content)
    data = server.parse_table_content(table[1])
    required_data = get_required_data(data, 3)
    if required_data:
        return beautify(generate_message(required_data))
    else:
        return beautify(get_nomain_message())


def notification():
    URL = "https://exams.keralauniversity.ac.in/Login/check1"
    server = SwordFish(URL)
    content = server.get_divContent()
    table = server.get_table_content(content)
    data = server.parse_table_content(table[1])
    required_data = get_required_data(data, 3)
    if required_data:
        return beautify(generate_message(required_data))
    else:
        return beautify(get_nomain_message())
