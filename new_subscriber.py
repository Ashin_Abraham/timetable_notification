import os
from apscheduler.schedulers.blocking import BlockingScheduler
from mailing import SendEmail
from main import timetable, notification
from utils import configure_bugsnag


mail = SendEmail()
mail.send_mail("Time Table", timetable())
mail.send_mail("Notification", notification())
