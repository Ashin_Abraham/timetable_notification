from apscheduler.schedulers.blocking import BlockingScheduler

sched = BlockingScheduler()


@sched.scheduled_job('interval', minutes=1)
@sched.scheduled_job('interval', minutes=2)
def Test1():
    print "Test1"


@sched.scheduled_job('interval', minute=1)
# @sched.scheduled_job('interval', minutes=2)
def Test2():
    print "Test2 one min"


# @sched.scheduled_job('interval', minutes=1)
@sched.scheduled_job('interval', minutes=2)
def Test3():
    print "Test3 two min"



sched.start()
