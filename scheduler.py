import os
from apscheduler.schedulers.blocking import BlockingScheduler
from mailing import SendEmail
from main import timetable, notification
from utils import configure_bugsnag

PROJECT_ROOT = os.path.abspath(os.path.dirname(__name__))
PROJECT_DIRNAME = PROJECT_ROOT.split(os.sep)[-1]


configure_bugsnag(PROJECT_DIRNAME)
sched = BlockingScheduler()


def TimetableMailingScheduler():
    mail = SendEmail()
    mail.send_mail("Time Table", timetable())
    print "Mail Status"


def NotificationMailingScheduler():
    mail = SendEmail()
    mail.send_mail("Notification", notification())
    print "Mail Status"


@sched.scheduled_job('cron', hour=18)
def timetableScheduler():
    print "Starting Sending Timetable"
    TimetableMailingScheduler()
    print "End Cron"


@sched.scheduled_job('cron', hour=6)
@sched.scheduled_job('cron', hour=18)
def notificationScheduler():
    print "Starting Sending Notification"
    NotificationMailingScheduler()
    print "End Cron"

sched.start()
