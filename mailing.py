from utils import configure_bugsnag
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from subscribers import SUBSCRIBERS


configure_bugsnag()

MAIL_CONF = {
    'from_addr': 'examnotification00@gmail.com',
    'password': 'notification123$'
}


class SendEmail():

    def __init__(self):
        smtp_host = 'smtp.gmail.com'
        smtp_port = 587
        self.from_addr = MAIL_CONF.get('from_addr')
        self.to_addrs = SUBSCRIBERS
        self.password = MAIL_CONF.get('password')
        try:
            self.server = smtplib.SMTP()
            self.server.connect(smtp_host, smtp_port)
            self.server.ehlo()
            self.server.starttls()
            self.server.login(self.from_addr, self.password)
        except smtplib.SMTPException as e:
            print e.message
            self.server = None
        except smtplib.SMTPAuthenticationError as e:
            print e.message
            self.server = None

    def send_mail(self, subject, message):
        for subscriber in self.to_addrs:
            print "Sending Mail to ", subscriber
            msg = MIMEMultipart()
            msg['From'] = "KU/KTU Notification" + self.from_addr
            msg['To'] = subscriber
            msg['Subject'] = subject
            msg.attach(MIMEText(message, 'html'))
            try:
                self.server.sendmail(
                    self.from_addr, subscriber, msg.as_string())
                print "=== Mail Send Successfully =="

            except smtplib.SMTPRecipientsRefused as e:
                print e.message
                print "== Mail Send Failed =="

            except smtplib.SMTPDataError as e:
                print e.message
                print "== Mail Send Failed =="

            except smtplib.SMTPSenderRefused as e:
                print e.message
                print "== Mail Send Failed =="

        return
