FROM python:2.7.15-alpine3.8
ENV PROJECT_ROOT=timetable_notification
ADD . /$PROJECT_ROOT
RUN set -ex \
    && pip install -r $PROJECT_ROOT/requirements.txt
WORKDIR $PROJECT_ROOT

CMD ["python", "swordfish.py"]
