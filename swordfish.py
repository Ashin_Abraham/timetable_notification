from utils import configure_bugsnag, urlify
from bs4 import BeautifulSoup
from urllib2 import urlopen

configure_bugsnag()
URL = 'https://exams.keralauniversity.ac.in/Login/check1'


class SwordFish(object):
    soup = None

    def __init__(self, url=URL):
        response = urlopen(url)
        if response.getcode() == 200:
            source = response.read()
            self.soup = BeautifulSoup(source, 'html.parser')
            response.close()

    def get_divContent(self):
        return self.soup.find('div', {'class': 'divContent'})

    def get_text(self, element, tag='h1'):
        try:
            return getattr(element, tag).getText().strip()
        except:
            return ''

    def get_table_content(self, element):
        try:
            table = element.findAll('table')
            return table
        except:
            return []

    def parse_table_content(self, table):
        t_row = table.findAll('tr')
        data = []
        row_data = {}
        for row in t_row:
            if 'tableHeading' in row.attrs.get('class', []):
                if row_data:
                    data.append(row_data)
                    row_data = {}
                row_data['tableHeading'] = row.getText().strip()
                row_data['displayList'] = []
            if 'displayList' in row.attrs.get('class', []):
                row_data['displayList'].append(
                    {
                        'entry': row.getText().strip(),
                        'link': urlify(row.find('a').attrs.get('href'))
                    }
                )
        if row_data:
            data.append(row_data)
            row_data = {}
        return data
