import os
import logging
import bugsnag
from datetime import datetime
from bugsnag.handlers import BugsnagHandler
from string import Template
from urllib import quote
import re


PROJECT_ROOT = os.path.abspath(os.path.dirname(__name__))
PROJECT_DIRNAME = PROJECT_ROOT.split(os.sep)[-1]


def configure_bugsnag(dirname=PROJECT_DIRNAME):
    bugsnag.configure(
      api_key="37856417c08f2a5ffdf684e1fd9e6d9d",
      project_root=dirname,
    )


def configure_bugsnag_logger():
    configure_bugsnag(PROJECT_DIRNAME)
    logger = logging.getLogger('swordfish')
    handler = BugsnagHandler()
    handler.setLevel(logging.ERROR)
    logger.addHandler(handler)
    return logger


def get_date_obj(date_str):
    try:
        return datetime.strptime(date_str, '%d/%m/%Y').date()
    except:
        return None


def urlify(string):
    return quote(string, "://")


def get_required_data(data, days):
    final = []
    today = datetime.now().date()

    for i in data:
        heading = i.get('tableHeading')
        posted_date = get_date_obj(heading.split()[-1])
        if (today - posted_date).days < days:
            final.append(i)
    if not final:
        return [data[0]]
    return final


def generate_message(data):
    message = "<p><b>Hi Sir/Madam,</b></p>"

    heading = Template('<h3><b><i>$tableHeading</i></b></h3>')
    table_content = Template("""<p>$SI)&nbsp;&nbsp;$context
                                <a href=$link> Download </a>
                              </p>""")
    for entry in data:
        message += heading.substitute(tableHeading=entry.get('tableHeading'))

        for si, content in enumerate(entry.get('displayList', []), 1):
            message += table_content.substitute(SI=si,
                                                context=content.get('entry', ''),
                                                link=content.get('link', ''))

    html = Template("""<html>
                    <head>
                    <style>
                    a:link {
                        color: green;
                        background-color: transparent;
                        text-decoration: none;
                    }
                    a:visited {
                        color: pink;
                        background-color: transparent;
                        text-decoration: none;
                    }
                    a:hover {
                        color: red;
                        background-color: transparent;
                        text-decoration: underline;
                    }
                    a:active {
                        color: yellow;
                        background-color: transparent;
                        text-decoration: underline;
                    }
                    </style>
                    </head>
                    <body>$message</body>
                    </html>""")

    return html.substitute(message=message)


def get_nomain_message():
    message = """
        <p><b>Hi Sir,</b></p>
        <h2>No New Message Posted</h2>
    """
    return message


def beautify(text):
    enc_txt = (re.sub('\s', ' ', text)).encode('ascii', 'ignore')
    return re.sub(' +', ' ', enc_txt)
